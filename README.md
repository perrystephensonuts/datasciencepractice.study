# 94692 Data Science Practice

This repo contains the course notes for **Data Science Practice**, an elective
subject within the **Master of Data Science and Innovation (MDSI)** program at 
the **University of Technology, Sydney (UTS)**.

The notes are written in R Markdown and published using the
[**bookdown**](https://github.com/rstudio/bookdown) package. You can view the 
compiled book at **https://datasciencepractice.study**

